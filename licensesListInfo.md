name of the file containing the template (templates are in folder ./licenseschecker/static/licenses_tmplt/*.tmplt) | license name
---|---
42 | BBN-Corporation (similar to Modified-BSD but with the "non-endorsement clause")
68 | CMU (similar to ISC)
74 | CSL (similar to ISC)
76 | CSL (similar to ISC) T2
22 | Custom-BSD (Similar to Modified-BSD)
12 | Custom-BSD (similar to Modified-BSD but without the "binary clause")
18 | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause")
35 | Custom-BSD (similar to Simplified-BSD but without the "binary clause")
53 | Custom-CMU (Free license)
63 | Custom-Free-License (Angelos D. Keromytis) or GPL (any version)
46 | Custom-Free-License (Damien Miller) or GPL (any version)
60 | Custom-Free-License (Paul-Popelka)
47 | Custom-Free-License (similar to Expat)
54 | Custom-Free-License (similar to X11; but without advertising)
34 | Custom-Modified-BSD (third clause is a "non-endorsement clause")
51 | Custom-Modified-BSD (third clause is a "non-endorsement clause" T2)
72 | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3)
71 | Custom-Modified-BSD (3rd clause is an endorsement clause)
38 | Custom-Modified-BSD (third clause is the "advertising clause")
64 | Custom-Modified-BSD (with differents names for copyrights)
79 | Custom-Modified-BSD (with differents names for copyrights) T2
81 | Custom-Modified-Expat
69 | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause)
86 | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs
6 | Custom-Original-BSD (four clause is a "non endorsement clause")
28 | Custom-Original-BSD (four clause is a "non-endorsement clause")
88 | Custom-Original-BSD (four clause is a "non endorsement clause") T for multiples contribs
11 | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses)
2 | Custom-Original-BSD (third and four clauses are custom clauses)
1 | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause")
25 | Custom-Original-BSD (third clause is a "non endorsement clause" and four clause is the "advertising clause") T1
84 | Custom-Simplified-BSD (Addition of "first lines of the file unmodified ..." in the 1st clause)
0 | Custom-Simplified-BSD (John S. Dyson; similar to Simplified-BSD with inclusion of unusual 2 clauses)
40 | Custom-Simplified-BSD (John S. Dyson; similar to Simplified-BSD with inclusion of unusual 3 clauses)
29 | Custom-Simplified-BSD (second clause is a "non-endorsement clause")
62 | Custom-Simplified-BSD (second clause is an "endorsement clause")
52 | Expat
5 | Expat or GPL-2
4 | GPL
15 | GPL-2 or Expat
10 | Hewlett-Packard (similar to Original-ISC but without endorsement clause)
17 | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README)
61 | Mazieres-BSD-style
82 | Old-Expat (with legal disclaimer 2)
83 | Old-Expat (without disclaimer 2)
59 | Modified-BSD
21 | Modified-BSD T2
58 | Modified-BSD or GPL
44 | Modified-BSD or GPL-2
77 | OSF (similar to ISC and CMU)
41 | Old-Expat (with legal disclaimer)
65 | Original-BSD
57 | Original-ISC
32 | Original-ISC ("and" is used instead of "and/or")
73 | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use")
23 | Public-Domain (Andrew Moon)
50 | Public-Domain (Artur Grabowski)
8 | Public-Domain (Christian Weisgerber)
30 | Public-Domain (Colin Plumb)
43 | Public-Domain (D. J. Bernstein)
16 | Public-Domain (Dale Rahn)
33 | Public-Domain (J.T. Conklin)
27 | Public-Domain (Matthew Dempsky)
48 | Public-Domain (Pedro Martelletto)
31 | Public-Domain (Ron Rivest)
49 | Public-Domain (Steve Reid)
9 | Public-Domain (Ted Unangst)
20 | Public-Domain (Theo de Raadt)
45 | Public-Domain (Todd C. Miller)
67 | Public-Domain (Todd C. Miller) T2
37 | Public-Domain (Vincent Rijmen, Antoon Bosselaers and Paulo Barreto)
39 | Public-Domain (notified in file)
13 | RTMX (similar to Simplified-BSD)
14 | Simplified-BSD
3 | Simplified-BSD {generated by versionmacro}
19 | Spencer-94
26 | TRW-Financial-Systems (similar to Expat)
55 | zlib
75 | __Nonfree-License!!!__ (CSL disclaimer only)
78 | __Nonfree-License!!!__ (Custom-BSD license) T2
80 | __Nonfree-License!!!__ (Custom license) Bonito Register Map
36 | __Nonfree-License!!!__ (John R. Hauser)
66 | __Nonfree-License!!!__ (Martin Birgmeier)
56 | __Nonfree-License!!!__ (Redistrib of object code only)
