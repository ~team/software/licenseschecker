from difflib import SequenceMatcher
from subprocess import getoutput, run
import os
import signal
import string
import sys


def double_check_matchs(FTC_head, matched_licenses):
    legend = 'Red = \033[0;31mnot matched\033[0m\n'
    legend += 'Green = \033[0;32mmatched\033[0m\n'
    for matched_licenses_list in matched_licenses.values():
        for matched_license in matched_licenses_list:
            with open(matched_license.full_path) as TMPLT:
                TMPLT_content = TMPLT.readlines()
            TMPLT_HEAD = ''.join(TMPLT_content[:4])
            TMPLT_content = ''.join(TMPLT_content[4:]).lower()
            RTP = legend + '\n'
            RTP += TMPLT_HEAD + '\n'
            matchs = SequenceMatcher(isjunk=lambda x: x in string.whitespace+string.punctuation, a=FTC_head, b=TMPLT_content, autojunk=False).get_matching_blocks()
            matchs.reverse()
            i = 0
            for match in matchs:
                if match.size == 0:
                    del matchs[i]
                i += 1
            matchs.reverse()
            i = 0
            for match in matchs:
                if i != 0:
                    RTP += f'\033[0;31m{FTC_head[matchs[i-1].a+matchs[i-1].size:match.a-1]}\033[0m'
                else:
                    RTP += f'\033[0;31m{FTC_head[0:match.a-1]}\033[0m'
                RTP += f'\033[0;32m{FTC_head[match.a-1:match.a+match.size]}\033[0m'
                i += 1
            endOfMatchPos = matchs[-1].a + matchs[-1].size
            FTC_head = FTC_head[endOfMatchPos:]
    RTP += f'\033[0;31m{FTC_head[matchs[-1].a+matchs[-1].size:]}\033[0m'
    i=0
    for RTP_line in RTP.splitlines():
        print(RTP_line)
        i += 1
        if i % 20 == 0:
            input('Press any key to continue to print the check')


def getUserInput(text, possibleInputs=['', 'y', 'n']):
    resp = input(text).strip().lower()
    while resp not in possibleInputs:
        print("\033[0;31mBad input !!\n Accepted inputs are" +
              f"{possibleInputs}\033[0m\n")
        resp = input(text).strip().lower()
    return resp


def executeThisCmdInANewTermAndReturnPID(terminal_manager_cmd, title, cmd):
    def __get_PID(escaped_cmd):
        __cmd = f"ps aux | grep '{escaped_cmd}' | awk "+"'{print$2}'"
        return getoutput(__cmd).strip()
    # don't open less 2 times for the same license type
    title = title.replace('"', '')
    escaped_cmd = f'[{cmd[0]}]' + cmd[1:].replace('"', '')
    cmd = f"""xterm -e "{terminal_manager_cmd} -t '{title}' -e '{cmd}'" &"""
    if (__get_PID(escaped_cmd) == ''):
        try:
            run(cmd, check=True, shell=True)
        except FileNotFoundError:
            print('pls set the value of terminal_manager_cmd in the conf.ini file')
            sys.exit()
        #pid = int(__get_PID(escaped_cmd))
        return int(__get_PID(escaped_cmd))


def kill_process(PROC_ID):
    try:
        os.kill(PROC_ID, signal.SIGKILL)
    except:
        print('less already closed')


def kill_processs(procsID):
    for procID in procsID:
        try:
            os.kill(procID, signal.SIGKILL)
        except:
            print('less already closed')
