from dataclasses import dataclass
from .licenseTemplate import LicenseTemplate


@dataclass
class MatchedLicense(LicenseTemplate):
    end_pos: int = 0
