import fileinput
import sys
from dataclasses import dataclass, field
from models.licenseTemplate import LicenseTemplate
# from models.fileForDoubleCheckLicenseType import\
#  FileForDoubleCheckLicenseType


@dataclass
class FileToCheck:

    Files_to_check = dict()
    orig_path: str
    full_path: str
    path: str
    file_type: str
    licenses: list = field(default_factory=list)
    description_Status: str = '.'
    isNonTrivial: bool = False
    isNonFree: bool = False
    more: str = '.'

    def __post_init__(self):
        FileToCheck.Files_to_check[self.path] = self

    @staticmethod
    def load(__file_list_path, __folder_path):
        with open(__file_list_path, encoding='utf-8') as __f:
            # ignore the two first lines cause it's .md
            __f.readline()
            __f.readline()
            for __line in __f:
                if __line.count(' | ') == 4:
                    __infos = __line.split(' | ')
                    __orig_path = __infos[0]
                    __path = __orig_path.replace('`', '').\
                        replace('*', '').strip()
                    __full_path = __folder_path + __path
                    __file_type = __infos[1].strip()
                    if __infos[2].strip() == '.':
                        __licenses = []
                    else:
                        __licenses = __infos[2].strip().split(' + ')
                    __description_Status = __infos[3].strip()
                    __number_of_dots_in_line = __line.count(' .')
                    if __number_of_dots_in_line > 1:
                        if True:
                            FileToCheck.Files_to_check[__path] =\
                                FileToCheck(__orig_path, __full_path,
                                            __path, __file_type,
                                            __licenses, __description_Status)
                    # else:
                        # if __licenses not in
                        #        FileForDoubleCheckLicenseType.Files_for_double_check_license_type:
                        #   if '+' not in __licenses:
                        #       FileForDoubleCheckLicenseType.\
                        #           Files_for_double_check_license_type[__licenses] = \
                        #               FileForDoubleCheckLicenseType(__licenses, __path,
                        #                                    __         description_Status)

    def licenses_to_str(self):
        if isinstance(self.licenses[0], LicenseTemplate):
            licenses_str = ''
            for license in self.licenses:
                licenses_str += f'{license.name} + '
            licenses_str = licenses_str.rstrip(' + ')
        else:
            licenses_str = ' + '.join(self.licenses)
        self.licenses = licenses_str

    def __str__(self):
        if self.isNonTrivial is True:
            print("\n\033[0;31m" +
                  'The license of this file is Non-Trivial' +
                  ' ** will be added at the start and at the end of the path' +
                  "\033[0m")
        elif self.isNonFree is True:
            print("\n\033[0;31m" +
                  'The license of this file is Nonfree' +
                  ' ** will be added at the start and at the end of the path' +
                  "\033[0m")

        return f"Path=\033[0;32m{self.path}\033[0m," +\
               f" fileType=\033[0;32m{self.file_type}\033[0m," +\
               f" license=\033[0;32m{self.licenses}\033[0m," +\
               f" descriptions/status=\033[0;32m{self.description_Status}" +\
               "\033[0m."

    def toMdString(self):
        if (self.isNonTrivial is False
                and self.isNonFree is False):
            toMdString = f'`{self.path}`'
        else:
            toMdString = f'**`{self.path}`**'
        return f'{toMdString} | {self.file_type} | {self.licenses}' +\
               f' | {self.description_Status} | {self.more}\n'

    def save(self, md_files_list_path):
        valueToWrite = self.toMdString()
        id = self.orig_path.strip()
        for line in fileinput.input(md_files_list_path, inplace=1):
            if line.split(' | ')[0].strip() == id:
                line = valueToWrite
            sys.stdout.write(line)
    
