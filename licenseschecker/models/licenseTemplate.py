from dataclasses import dataclass, field
import glob
import re
import string
from .template import Template
from typing import NewType



@dataclass
class LicenseTemplate:
    License_templates = dict()
    full_path: str
    name: str
    status: str
    template: Template = None
    templates: list = field(default_factory=list)
    uncompatible_matchs: list = field(default_factory=list)
    invalidating_strings: list = field(default_factory=list)
    

    def __post_init__(self):
        LicenseTemplate.License_templates[self.name] = self

    def __str__(self):
        return f'Name=\033[0;32m{self.name}\033[0m'\
             + f', template=\033[0;32m{self.template}\033[0m'\
             + f', templates=\033[0;32m"{self.templates}\033[0m'\
             + f', status=\033[0;32m{self.status}\033[0m'\
             + ', uncompatible_matchs=\033[0;32m'\
             + f'{self.uncompatible_matchs}\033[0m'\
             + ', invalidating_strings=\033[0;32m'\
             + f'{self.invalidating_strings}\033[0m'

    @staticmethod
    def load(__licenses_templates_path):
        pattern = r'(?<=\[).+?(?=\])'
        for __file in glob.glob(__licenses_templates_path+'*.tmplt'):
            with open(__file, encoding='utf-8') as __f:
                __full_path = __file
                __name = __f.readline().strip()
                __uncompatible_matchs = re.findall(pattern, __f.readline().strip().lower())
                __invalidating_strings = re.findall(pattern, __f.readline().strip().lower())
                __status = __f.readline().strip()
                __template = __f.read().lower()
                __template = __template.translate(
                                str.maketrans('',
                                              '',
                                              string.punctuation
                                              + string.whitespace))
                if 'deletionhereitis' in __template:
                    __parts_template = __template.split('deletionhereitis')
                    __templates = []
                    for __template in __parts_template:
                        __size = len(__template)
                        __templates.append(Template(__template, __size))
                        LicenseTemplate(__full_path, __name, __status,
                                        templates=__templates,
                                        uncompatible_matchs=__uncompatible_matchs,
                                        invalidating_strings=__invalidating_strings)
                else:
                    LicenseTemplate(__full_path, __name, __status, Template(__template, len(__template)),
                                    uncompatible_matchs= __uncompatible_matchs,
                                    invalidating_strings=__invalidating_strings)
