from dataclasses import dataclass


@dataclass
class FileForDoubleCheckLicenseType:

    Files_for_double_check_license_type = dict()
    license : str
    path : str
    description_Status : str
    
    def __post_init__(self):
        FileForDoubleCheckLicenseType.Files_for_double_check_license_type[self.license] = self
