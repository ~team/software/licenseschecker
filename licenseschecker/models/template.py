from dataclasses import dataclass


@dataclass
class Template:
    chars: str
    size: int
