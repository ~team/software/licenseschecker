from models.licenseTemplate import LicenseTemplate
from view import view


def FileStatusStepController(FTC):

    def askUserForCustomStatusAndSetIt(FTC):
        resp = input('pls type the status description to set:')
        FTC.description_Status = resp.strip()

    def askUserForLicenseStatusAndSetIt(FTC):
        resp = view.getUserInput('is license status '
                                 '[OK/OK (Combined Licenses)/Custom]: ',
                                 ['ok', 'ok (combined licenses)', 'custom'])
        if resp == 'ok':
            FTC.description_Status = 'OK'
        elif resp == 'ok (combined licenses)':
            FTC.description_Status = 'OK (Combined Licenses)'
        elif resp == 'custom':
            askUserForCustomStatusAndSetIt(FTC)

    def manage_unlicensed_DS(FTC):

        def manage_non_trivial_DS(FTC,
                                  nb_lines_total,
                                  nb_non_empty_lines):
            print('\nDo you want to set the file status to:')
            resp = view.getUserInput('\033[0;32mNon-Trivial\033[0m; '
                                     f'\033[0;32m{nb_non_empty_lines}\033[0m '
                                     'non empty lines; '
                                     f'\033[0;32m{nb_lines_total}\033[0m '
                                     'total lines [Y/n]:')
            if resp == 'n':
                askUserForCustomStatusAndSetIt(FTC)
            else:
                FTC.description_Status = 'Non-Trivial; '\
                                         f'{nb_non_empty_lines} '\
                                         'non empty lines; '\
                                         f'{nb_lines_total} total lines'

        def manage_trivial_DS(FTC, lines, nb_lines_total):
            is_comment_block = False
            commented_lines = 0
            code_lines = 0
            commented_lines2 = 0
            code_lines2 = 0
            for line in lines:
                line = line.strip()
                if line != '':
                    if is_comment_block is False:
                        if line.startswith('/*'):
                            is_comment_block = True
                            commented_lines += 1
                            commented_lines2 += 1
                            if '*/' in line:
                                is_comment_block = False
                        elif line.startswith('#'):
                            code_lines += 1
                            commented_lines2 += 1
                        else:
                            code_lines += 1
                            code_lines2 += 1
                    elif line.endswith('*/'):
                        commented_lines += 1
                        is_comment_block = False
                    elif line[0] == '*':
                        commented_lines += 1
            print('\n\033[0;42mMaybe\033[0m:')
            print(f'\033[0;32mTrivial\033[0m;'
                  f'\033[0;32m {code_lines} \033[0mcode lines\033[0;32m '
                  f'+ {commented_lines}\033[0m commented lines;'
                  f'\033[0;32m {nb_lines_total}\033[0m total lines')
            print('or')
            print(f'\033[0;32mTrivial\033[0m;'
                  f'\033[0;32m {code_lines2} \033[0mcode lines\033[0;32m '
                  f'+ {commented_lines2}\033[0m commented lines;'
                  f'\033[0;32m {nb_lines_total}\033[0m total lines')
            print('\npls doublecheck the ahead lines')

        with open(FTC.full_path, encoding='utf-8') as f:
            file_content = f.read()
        lines = file_content.strip().split("\n")
        nb_lines = 0
        nb_non_empty_lines = 0
        for line in lines:
            nb_lines += 1
            if line.strip() != '':
                nb_non_empty_lines += 1
        if nb_non_empty_lines > 15:
            FTC.isNonTrivial = True
            manage_non_trivial_DS(FTC, nb_lines, nb_non_empty_lines)
        else:
            manage_trivial_DS(FTC, lines, nb_lines)
            askUserForCustomStatusAndSetIt(FTC)

    def set_description_status(FTC):
        while True:
            try:
                if FTC.isNonFree:
                    print('__Nonfree!!!__ [Why license is Nonfree]')
                    askUserForCustomStatusAndSetIt(FTC)
                elif 'Unlicensed!!!' in FTC.licenses[0]:
                    manage_unlicensed_DS(FTC)
                else:
                    askUserForLicenseStatusAndSetIt(FTC)
                break
            except KeyboardInterrupt as e:
                resp = view.getUserInput('\nDo you want to '
                                         'retry to do this step ?\n[Y/n]:')
                if resp == 'n':
                    raise KeyboardInterrupt from e
    
    for license in FTC.licenses:
        if isinstance(license, LicenseTemplate):
            if 'nonfree' in license.name.lower():
                FTC.isNonFree = True
        else:
            if 'nonfree' in license.lower():
                FTC.isNonFree = True
    if len(FTC.licenses) == 1:
        if isinstance(FTC.licenses[0], LicenseTemplate) is False:
            set_description_status(FTC)
        else:
            FTC.description_Status = FTC.licenses[0].status
    else:
        if FTC.isNonFree:
            set_description_status(FTC)
        else:
            resp = view.getUserInput('Do you want to set the license status to '
                                     '\033[0;32m OK (Combined Licenses)\033[0m'
                                     '\n[Y/n]:')
            if resp != 'n':
                FTC.description_Status = 'OK (Combined Licenses)'
            else:
                askUserForCustomStatusAndSetIt(FTC)
