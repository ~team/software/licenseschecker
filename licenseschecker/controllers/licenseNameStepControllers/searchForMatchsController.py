from models.matchedLicense import MatchedLicense


def FindAllPos(stringToMatch, target) -> list:
    index = 0
    matchs_pos = []
    while True:
        match_pos = target.find(stringToMatch, index)
        if match_pos == -1:
            return matchs_pos
        matchs_pos.append(match_pos)
        index = match_pos + len(stringToMatch)


def SearchForMatchsController(head, license_templates, matchedLicenses):

    def isTemplatePartsMatchAndWhere(strings_to_match, target, start_index)\
            -> list:
        pos_of_parts_found = []
        for template in strings_to_match:
            pos_of_part_found = target.find(template.chars, start_index)
            if pos_of_part_found == -1:
                return -1
            if (len(pos_of_parts_found) != 0
                    and pos_of_part_found <= pos_of_parts_found[-1]):
                return -1
            pos_of_parts_found.append(pos_of_part_found)
            start_index = pos_of_part_found + template.size
        return pos_of_parts_found

    for license_template in license_templates:
        if license_template.template is not None:
            posOfLicenseFounds =\
                FindAllPos(license_template.template.chars, head)
            if len(posOfLicenseFounds) != 0:
                for posOfLicenseFound in posOfLicenseFounds:
                    matchedLicense =\
                        MatchedLicense(license_template.full_path,
                                       license_template.name,
                                       license_template.status,
                                       license_template.template,
                                       license_template.templates,
                                       license_template.uncompatible_matchs,
                                       license_template.invalidating_strings,
                                       posOfLicenseFound
                                       + license_template.template.size)
                    matchedLicenses[posOfLicenseFound].append(matchedLicense)
        else:
            start_index = 0
            while True:
                templatePartsMatchsPos = isTemplatePartsMatchAndWhere(
                                            license_template.templates,
                                            head, start_index)
                if templatePartsMatchsPos != -1:
                    j = 0
                    isTemplatePartsMatch = True
                    for pos_of_part_found in templatePartsMatchsPos:
                        if j != 0:
                            sizeOfPrecedentPart =\
                                license_template.templates[j-1].size
                            nbOfCharsBetweenMatchs = pos_of_part_found \
                                - templatePartsMatchsPos[j-1] \
                                - sizeOfPrecedentPart
                            if nbOfCharsBetweenMatchs > 75:
                                isTemplatePartsMatch = False
                                start_index = templatePartsMatchsPos[0] \
                                    + license_template.templates[0].size
                                break
                        j += 1
                    if isTemplatePartsMatch:
                        matchedLicense =\
                            MatchedLicense(license_template.full_path,
                                           license_template.name,
                                           license_template.status,
                                           license_template.template,
                                           license_template.templates,
                                           license_template.
                                           uncompatible_matchs,
                                           license_template.
                                           invalidating_strings,
                                           templatePartsMatchsPos[-1]
                                           + license_template.
                                           templates[-1].size)
                        matchedLicenses[templatePartsMatchsPos[0]]\
                            .append(matchedLicense)
                        start_index = templatePartsMatchsPos[-1] \
                            + license_template.templates[-1].size
                else:
                    break
