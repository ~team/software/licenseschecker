import os
import string
from tempfile import NamedTemporaryFile
from .searchForMatchsController\
    import FindAllPos
from exceptions.goToTheEndOfMainLoopException\
    import GoToTheEndOfMainLoopException
from view import view


def Count_number_of_matchs(matchedLicenses):
    __nb_matchs = 0
    for matchs in matchedLicenses.values():
        __nb_matchs += len(matchs)
    return __nb_matchs


def ManageMatchsController(FTC, orig_head, head,
                           matchedLicenses, terminal_manager_cmd,
                           processIdToKillATheEndOfThisFileChk):

    def clean_results(matchedLicenses):
        def remove_match(pos, match):
            if len(matchedLicenses[pos]) > 1:
                matchedLicenses[pos].remove(match)
            else:
                del matchedLicenses[pos]

        def check_for_uncompatible_matchs(pos_of_matchs,
                                          matchs_at_this_pos):
            matched_licenses_names =\
                [matchedLicense.name.lower()
                    for matchedLicense in matchs_at_this_pos]
            for matchedLicense in matchs_at_this_pos:
                for uncompatible_match in\
                        matchedLicense.uncompatible_matchs:
                    if uncompatible_match in matched_licenses_names:
                        remove_match(pos_of_matchs, matchedLicense)
                        break

        def check_for_invalidating_strings(pos_of_matchs,
                                           matchs_at_this_pos):
            for matchedLicense in matchs_at_this_pos:
                __is_matched_license_valid = True
                for __invalidating_string in\
                        matchedLicense.invalidating_strings:
                    __invalidating_string =\
                        __invalidating_string\
                        .translate(str.maketrans
                                   ('', '', string.punctuation
                                    + string.whitespace)).lower()
                    __invalidating_string_positions =\
                        FindAllPos(__invalidating_string, head)
                    for __invalidating_string_pos in\
                            __invalidating_string_positions:
                        if __invalidating_string_pos != -1:
                            if __invalidating_string_pos in range(
                                                        pos_of_matchs-250,
                                                        pos_of_matchs):
                                if len(matchedLicenses[pos_of_matchs]) > 1:
                                    matchedLicenses[pos_of_matchs]\
                                        .remove(matchedLicense)
                                else:
                                    return pos_of_matchs
                                __is_matched_license_valid = False
                                break
                    if __is_matched_license_valid is False:
                        break
        keys_to_delete = []
        for pos, matchs in matchedLicenses.items():
            check_for_uncompatible_matchs(pos, list(matchs))
            keys_to_delete\
                .append(check_for_invalidating_strings(pos, list(matchs)))
        for key_to_delete in keys_to_delete:
            if key_to_delete is not None:
                del matchedLicenses[key_to_delete]

    clean_results(matchedLicenses)
    __nb_matchs = Count_number_of_matchs(matchedLicenses)
    if __nb_matchs == 0:
        return 'All match(s) invalidated'
    print(f'\n{__nb_matchs} match(s) for license of file:\t'
          f"\033[0;35m{FTC.path}\033[0m")
    print("Pls confirm:\n")
    resultToprint = ''
    matchedLicenses = dict(sorted(matchedLicenses.items(),
                                  key=lambda item: item[0]))
    #RTP = view.double_check_matchs(orig_head, matchedLicenses)
    for posOfLicenseFound in matchedLicenses:
        matchsAtThisPos = ''
        for matchedLicense in matchedLicenses[posOfLicenseFound]:
            __license_template_path = matchedLicense.full_path
            cmd = '(tput bold;'\
                + f'echo --- {__license_template_path};'\
                + 'tput sgr0;'\
                + f'head -n 4 {__license_template_path};'\
                + 'diff -U 75 -i -w --color=always'\
                + f' <(tail -n +5 {__license_template_path}) {FTC.full_path} | tail -n +2)'\
                + '| less -R'
            f = open("/tmp/licensechecker.sh", "w")
            f.write(cmd)
            f.close()
            lessProcessID = view.executeThisCmdInANewTermAndReturnPID(
                terminal_manager_cmd,
                'less of diff for double check '
                f"{matchedLicense.name}",
                'bash /tmp/licensechecker.sh')
            processIdToKillATheEndOfThisFileChk.append(lessProcessID)
            matchsAtThisPos += matchedLicense.name + ' or '
        resultToprint += matchsAtThisPos[:-4] + ' + '
    resultToprint = resultToprint[:-3]
    print(f"Is\033[0;35m {FTC.path} \033[0:m:")
    resp = view.getUserInput("license type =\033[0;32m"
                             f" {resultToprint}\033[0m\n[Y/n]:")
    if resp == 'n':
        resp = input('pls type the license '
                     'to set to this file or press'
                     ' enter to skip this file:')
        if resp == '':
            view.kill_processs(processIdToKillATheEndOfThisFileChk)
            raise GoToTheEndOfMainLoopException()
        FTC.licenses = resp.strip().split(' + ')
    else:
        if (resp in ['y', '']):
            posOfLicensesToSet = list(matchedLicenses.keys())
            if __nb_matchs != 1:
                for posOfLicenseToSet in posOfLicensesToSet:
                    if len(matchedLicenses[posOfLicenseToSet]) > 1:
                        print('\033[0;32mOr\033[0m in result mean '
                              'the tool suggested '
                              'many possible licenses'
                              'at the same place.\n'
                              '\033[0;31m'
                              "You can't validate it as this\033[0m")
                        return 'DoItAgain'
            for values in matchedLicenses.values():
                for license_template in values:
                    FTC.licenses.append(license_template)
    return 'OK'
