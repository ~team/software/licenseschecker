import collections
import string
from .manageMatchsController\
    import ManageMatchsController, Count_number_of_matchs
from .searchForMatchsController\
    import SearchForMatchsController
from exceptions.goToTheEndOfMainLoopException\
    import GoToTheEndOfMainLoopException
from models.licenseTemplate import LicenseTemplate
from view import view


def LicenseNameStepController(confs, FTC):

    def clean_orig_head(original_head) -> str:
        head = ''
        strings_to_match = ['echo:', 'ECHO:']
        for line in original_head.splitlines():
            for string_to_match in strings_to_match:
                if line.startswith(string_to_match):
                    line = line.lstrip(string_to_match)
            head += line + '\n'
        return head

    def format_original_head(head) -> str:
        head = head.translate(str.maketrans
                              ('', '',
                               string.punctuation
                               + string.whitespace))
        return head

    def license_name_step(FTC, head, matchedLicenses,
                          nb_non_empty_lines,
                          terminal_manager_cmd,
                          processIdToKillATheEndOfThisFileChk):
        state = ''
        if Count_number_of_matchs(matchedLicenses) != 0:
            while True:
                try:
                    state =\
                        ManageMatchsController(FTC, original_head, head, matchedLicenses,
                                               terminal_manager_cmd,
                                               processIdToKillATheEndOfThisFileChk)
                    if state in ['OK', 'All match(s) invalidated']:
                        if state == 'All match(s) invalidated':
                            license_name_step(FTC, matchedLicenses,
                                              nb_non_empty_lines,
                                              terminal_manager_cmd,
                                              processIdToKillATheEndOfThisFileChk)
                        break
                except KeyboardInterrupt as e:
                    resp = view.getUserInput('\nDo you want to '
                                             'retry to do this step ?\n[Y/n]:')
                    if resp == 'n':
                        view.kill_processs(processIdToKillATheEndOfThisFileChk)
                        raise KeyboardInterrupt from e
        else:
            while True:
                try:
                    state = manuallySetLicenseName(FTC, nb_non_empty_lines)
                    if state != 'DoItAgain':
                        break
                except KeyboardInterrupt as e:
                    resp = view.getUserInput('\nDo you want to retry to '
                                             'do this step ?\n[Y/n]:')
                    if resp == 'n':
                        view.kill_processs(processIdToKillATheEndOfThisFileChk)
                        raise KeyboardInterrupt from e

    def manuallySetLicenseName(FTC, nb_non_empty_lines):
        print('\nNo match(s) for license of file:\t'
              f'\033[0;35m {FTC.path}\033[0m')
        print("Pls set values manually:")
        if nb_non_empty_lines <= 15:
            print("\nCould be:\033[0;42m" + '"Unlicensed!!!"' + "\033[0m")
        else:
            print("\nCould be: "+"\033[0;42m" + '__Unlicensed!!!__'+"\033[0m")
        resp = input('pls type the license to set to this file '
                     'or press enter to skip it:')
        if resp == '':
            view.kill_processs(processIdToKillATheEndOfThisFileChk)
            raise GoToTheEndOfMainLoopException()
        FTC.licenses = resp.strip().split(' + ')
        return 'OK'

    matchedLicenses = collections.defaultdict(list)
    with open(FTC.full_path, encoding='utf-8') as f:
        file_content = f.read()
    nb_lines = 0
    nb_non_empty_lines = 0
    for line in file_content.strip().split("\n"):
        nb_lines += 1
        if line.strip() != '':
            nb_non_empty_lines += 1
    processIdToKillATheEndOfThisFileChk = []
    print(f'The file do\033[0;32m {nb_lines} \033[0mlines.')
    original_head = file_content[:int(confs['header_size'])].lower()
    original_head = clean_orig_head(original_head)
    head = format_original_head(original_head)
    LicenseTemplate.load(confs['license_templates_folder_path'])
    assert len(LicenseTemplate.License_templates) > 35,\
        '\033[0;31mBad conf\033[0m for '\
        '\033[0;31mlicense_templates path\033[0m: '\
        f'\033[0;31m{confs["license_templates_folder_path"]}\033[0m\n'\
        f'{len(LicenseTemplate.License_templates)} templates loaded'
    SearchForMatchsController(head,
                              LicenseTemplate.License_templates.values(),
                              matchedLicenses)
    license_name_step(FTC, head, matchedLicenses,
                      nb_non_empty_lines,
                      confs['terminal_manager_cmd'],
                      processIdToKillATheEndOfThisFileChk)
    view.kill_processs(processIdToKillATheEndOfThisFileChk)
