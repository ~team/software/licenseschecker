from static.fileTypeMatching import FileTypeMatching
from .context import view


def FileTypeStepController(FTC):

    def get_file_type(file_name):
        string_to_test = file_name[-20:]
        if string_to_test.rfind('/') != -1:
            string_to_test = string_to_test.rsplit('/', 1)[1]
        if string_to_test in FileTypeMatching:
            return FileTypeMatching[string_to_test]
        if string_to_test.rfind('.') != -1:
            extension = string_to_test.rsplit('.', 1)[1]
            if extension in FileTypeMatching:
                return FileTypeMatching[extension]
        return '?'

    def set_file_type(FTC, file_type):

        def askUserForFileType():
            print('\nfile type = \033[0;32m?\033[0m: ')
            resp = input('pls type the file type to set: ').strip()
            if resp == '':
                resp = '?'
            return resp

        if file_type == '?':
            FTC.file_type = askUserForFileType()
        else:
            resp = view.getUserInput("\nfile type = " + "\033[0;32m" +
                                     file_type + "\033[0m [Y/n]: ")
            if resp == 'n':
                resp = input('pls type the fileType to set :')
                FTC.file_type = resp.strip()
            else:
                FTC.file_type = file_type

    file_type = get_file_type(FTC.path)
    set_file_type(FTC, file_type)
