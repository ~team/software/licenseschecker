#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This tool is just for help, don't fully trust it, double check
def main(args):
    return 0

if __name__ == '__main__':
    import configparser
    import sys
    from controllers.licenseNameStepControllers.licenseNameStepController\
        import LicenseNameStepController
    from controllers.fileTypeStepController import FileTypeStepController
    from controllers.fileStatusStepController import FileStatusStepController
    from exceptions.goToTheEndOfMainLoopException\
        import GoToTheEndOfMainLoopException
    from models.fileToCheck import FileToCheck
    from view import view

    __app_folder = f'{__file__.rsplit("/", 1)[0]}'
    __conf_file_path = f'{__app_folder}/../conf.ini'
    __default_conf =\
        f"""
        [DEFAULT]
        terminal_manager_cmd = xfce4-terminal
        # full path to the md file and its table
        md_files_list_path = /home/user/list_of_files.md
        # folder of the licenses linked by name column of the md file
        files_to_check_folder_path = /home/user/hyperbola/investigation/license/hyperbk/
        # folder of the templates of licenses to use for checking header
        license_templates_folder_path = {__app_folder}/static/licenses_tmplt/
        # number of chars of the license file to check
        header_size = 7000
        """


    def __load_conf(__conf_file_path):
        __config = configparser.ConfigParser()
        __config.read(__conf_file_path)
        __app_conf = (__config['DEFAULT'])
        assert len(__app_conf) == 5,\
            'The content of the \033[0;31mconf.ini file\033[0m '\
            'at the root of this git folder should be as exemple:\n'\
            f'{__default_conf}\n'\
            f'\033[0;31mMissing conf values\033[0m:\n {__app_conf}'
        return __app_conf


    def do_steps(__app_conf, __file_to_check, __md_files_list_path, i):
        LicenseNameStepController(__app_conf, __file_to_check)
        FileStatusStepController(__file_to_check)
        FileTypeStepController(__file_to_check)
        __file_to_check.licenses_to_str()
        print(f'\nfile currents values are:\n{__file_to_check}\n')
        resp = view.getUserInput('Do you want to update '
                                 f"\033[0;35m{__file_to_check.orig_path}\033[0m in"
                                 f'\033[0;35m {__md_files_list_path}\033[0m\n'
                                 '[Y/n]')
        if resp != 'n':
            __file_to_check.save(__md_files_list_path)
            i += 1
        else:
            resp = view.getUserInput('\nDo you want to '
                                     'restart the check of this file ?\n[Y/n]:')
            if resp != 'n':
                print('-----'*2)
                __file_to_check.licenses = []
                do_steps(__app_conf, __file_to_check, __md_files_list_path, i)
        view.kill_process(__LESS_PID)
        if i and i % 25 == 0:
            print(f'WP my friend, {i} files were treated,\n'
                  'u press enter like no one else :)')


    __app_conf = __load_conf(__conf_file_path)
    __md_files_list_path = __app_conf.pop('md_files_list_path')
    __files_to_check_folder_path = __app_conf.pop('files_to_check_folder_path')
    FileToCheck.load(__md_files_list_path, __files_to_check_folder_path)
    assert len(FileToCheck.Files_to_check) != 0,\
        f'\033[0;31mNo files to check loaded\033[0m\n'\
        'pls check those paths '\
        'or maybe all the files to check in md table are checked:\n'\
        f'\033[0;35m{__md_files_list_path}\033[0m, '\
        f'\033[0;35m{__files_to_check_folder_path}\033[0m'
    i = 0
    for __file_to_check in FileToCheck.Files_to_check.values():
        try:
            __LESS_PID = view.executeThisCmdInANewTermAndReturnPID(
                __app_conf['terminal_manager_cmd'],
                'less ' + __file_to_check.full_path,
                'less ' + __file_to_check.full_path
            )
            do_steps(__app_conf, __file_to_check, __md_files_list_path, i)
        except ValueError:
            print(__file_to_check.full_path.encode())
        except GoToTheEndOfMainLoopException:
            view.kill_process(__LESS_PID)
        except KeyboardInterrupt:
            view.kill_process(__LESS_PID)
            sys.exit()
        input('Press a key to check next file')
        print('-----------------------------------------'
              '---------------------------------------\n')
